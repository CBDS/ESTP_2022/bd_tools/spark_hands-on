# Spark hands-on

## Onboarding Databricks
- Create an account at https://www.databricks.com/try-databricks
- Fill in your data
- GET STARTED FOR FREE
- (small, below) Get started with Community Edition
- (solve annoying puzzles)
- (solve them again, because you made a mistake)
- (small, below) Get started with Community Edition

## First Steps

- Create a cluster
- Attach the tutorial notebook
- Run all cells
- Read the cells and understand them roughly
- Click on the bar chart icon to produce charts
- Pat yourself on the back!

## MapReduce with Pride and Prejudice
Once upon a time...

An avid collector of Jane Austen works dropped their precious collection of 2000 books of "Pride and Prejudice". The impact changed one word among all those books. Which word was affected?

Log in on the Databricks Community website, then

- Left menu: Workspace
- Caret ˇ
- Import, URL `https://gitlab.com/CBDS/ESTP_2022/bd_tools/spark_hands-on/-/raw/main/word_count.ipynb`
- Replace `ACC` and `SEC` with the access- and security keys given to you in the presentation.
- Edit the notebook and find out which word was changed. Start with 10 books, it's faster.
- Then try 2000 books and find a different word.
- Bonus: In which line number? 

## Analyzing the world of Rrrrrr 🏴‍☠️🦜
R package popularity contest


- Left menu: Workspace
- Caret ˇ
- Import, URL `https://gitlab.com/CBDS/ESTP_2022/bd_tools/spark_hands-on/-/raw/main/R_packages.ipynb`
-  Which are Top 10 downloads from last Monday?
-  Bonus: Top 10 for last Month? Last year?
